var victor = {
    nombre: 'Victor',
    apellido: 'Tapia',
    edad: 24,
    ingeniero: true,
    cocinero: false,
    cantante: false,
    dj: false,
    guitarrista: false,
    gamer: true
}

function imprimirProfesiones(persona){
    console.log(`${persona.nombre} es:`)

    if(persona.ingeniero){
        console.log('Ingeniero')
    }

    if(persona.cocinero){
        console.log('Cocinero')
    }

    if(persona.cantante){
        console.log('Cantante')
    }
    if(persona.dj){
        console.log('DJ')
    }
    if(persona.guitarrista){
        console.log('Guitarrista')
    }
    if(persona.gamer){
        console.log('Gamer')
    }
}

const MAYORIA_DE_EDAD = 18

//var esMayorDeEdad = function (persona)
/*var esMayorDeEdad = persona => {
    return persona.edad >= MAYORIA_DE_EDAD
}*/

//var esMayorDeEdad = persona => persona.edad >= MAYORIA_DE_EDAD

const esMayorDeEdad = ({ edad }) => edad >= MAYORIA_DE_EDAD
const esMenorDeEdad = ({ edad }) => edad >= !esMayorDeEdad({edad})

function imprimirSiEsMayorDeEdad(persona){
    if(esMayorDeEdad(persona)){
        console.log('Sì es mayor de edad')
    }else{
        console.log('No es mayor de edad')
    }
}

function permitirAcceso(){
    if(!esMayorDeEdad(persona)){
        console.log(`Acceso denegado`)
    }
}