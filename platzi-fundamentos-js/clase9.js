var x = 4, y = '4'

// x == y -> True
// x === y -> False
// Triple igual conviene màs

var victor = {
    nombre: 'Victor'
}

var otraPersona = {
    nombre: 'Victor'
}

/* 
victor === otraPersona -> False

var otraPersona = victor

victor === otraPersona -> True
*/