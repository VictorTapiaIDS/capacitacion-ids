class Persona{
    constructor(nombre, apellido, altura){
        this.nombre = nombre
        this.apellido = apellido
        this.altura = altura
    }

    saludar(fn){
        console.log(`Hola, me llamo $(this.nombre) $(this.apellido)`)
        if(fn){
            fn(nombre, apellido)
        }
    }

    soyAlto(){
        return this.altura > 1.8
    }
}

class Desarrollador extends Persona{
    constructor (nombre, apellido, altura) {
        super(nombre, apellido, altura)
    }

    saludar(fn){
       console.log(`Hola, me llamo $(this.nombre) $(this.apellido) y soy desarrollador`)
       if(fn){
            fn(this.nombre, this.apellido, true)
        } 
    }
}


function responderSaludo(){
    console.log(`Buen día ${nombre} ${apellido}`)
    if (esDev){
        console.log(`Ah mira, no sabia que eras desarrollador`)
    }
}

 

//var victor = new Persona('Victor', 'Tapia', 1.78)
//var erika = new Persona('Erika', 'Luna', 1.6)
//var arturo = new Persona('Arturo', 'Martinez', 1.82)
